---
title: Advisory Board 🤝
description: Grey Software's Advisory Board 
category: Team
position: 4
advisoryBoard:
  - name: Jon
    avatar: https://avatars.githubusercontent.com/u/22453408?s=400&u=163ea1db63766e47a5bf0940f25be16653bffc42&v=4
    position: Full Stack Developer - Prodigy
    github: https://github.com/JonathanYcWang
    gitlab: https://gitlab.com/JonathanYcWang
    linkedin: https://www.linkedin.com/in/jonathan-yc-wang/
  - name: Haris
    avatar: https://avatars.githubusercontent.com/u/30947706?s=460&u=27d978a7a6e58626bb52f9bbb3cd8004e51bdc97&v=4
    position: Project Manager at LUMS
    github: https://github.com/harismuneer
    linkedin: https://www.linkedin.com/in/harismuneer/
---

## Advisory Board

<team-profiles :profiles="advisoryBoard"></team-profiles>